# terraform-infoblox

testing the official Infoblox Terraform provider

https://github.com/infobloxopen/terraform-provider-infoblox

# getting started

* requires Cloud Network Automation (CNA) license installed. Optionally, use a temp license by running `set temp_license` at Infoblox console
* **do not** use this against a production Infoblox. doing the wrong things with terraform can easily lead to data destruction or widespread misconfiguration 

## usage

1. download infoblox appliance / have infoblox available
1. configure server location and credentials in prereq.sh, `source prereq.sh` or set them yourself if you're a tough independant individual who doesn't need a script to tell you how to carry yourself
1. `terraform apply`
1. great success!
