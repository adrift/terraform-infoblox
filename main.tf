resource "infoblox_network_view" "view_10-10-0-0" {
  network_view_name = "10.10.0.0/21"
  tenant_id = "test"
}

resource "infoblox_network" "network_10-10-0-0" {
  network_view_name = "10.10.0.0/21"
  network_name = "10.10.0.0/21"
  cidr = "10.10.0.0/24"
  tenant_id = "test"
}

resource "infoblox_ip_allocation" "ip_10-10-0-0_myip" {
  network_view_name = "10.10.0.0/21"
  vm_name = "terraform-demo"
  cidr = infoblox_network.network_10-10-0-0.cidr
  tenant_id = "test"
}